import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn as sk
from sklearn.preprocessing import LabelEncoder
from sklearn.impute import SimpleImputer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from models import *
import random
from analyse import *

# Défini toutes les colonnes à garder dans le nettoyage des données
columns = ['B_fighter','R_fighter','title_bout',
                    'B_avg_BODY_landed', 'B_avg_HEAD_landed', 'B_avg_TD_att', 'B_avg_TOTAL_STR_landed', 
                'B_avg_opp_BODY_att', 'B_avg_opp_HEAD_landed', 'B_avg_opp_LEG_landed', 
                'B_avg_opp_SIG_STR_att', 'B_avg_opp_TOTAL_STR_att',
                
                'R_avg_BODY_landed', 'R_avg_HEAD_landed', 'R_avg_TD_att', 'R_avg_TOTAL_STR_landed', 
                'R_avg_opp_BODY_att', 'R_avg_opp_HEAD_landed', 'R_avg_opp_LEG_landed', 
                'R_avg_opp_SIG_STR_att', 'R_avg_opp_TOTAL_STR_att','Winner','weight_class']

# Permet d'inverser le coté des combattants
# Permet ainsi de ne pas prendre en compte dans l'apprentissage le coté de départ (rouge ou bleu)
def swap_values(row):
    # Choisi avec 1 chance sur 2 si le swap va être effectué
    if random.random() > 0.5:
        return swap_values_withoutran(row)
    return row

# Inverse le coté des combattants
def swap_values_withoutran(row):
    for column in columns:
        if column.startswith('B_'):
            opposite_column = 'R_' + column[2:]
            row[column], row[opposite_column] = row[opposite_column], row[column]
        if column.startswith('Winner'):
            if row[column] == 0:
                row[column] = 2
            elif row[column] == 2:
                row[column] = 0
    return row

# Récupere les données du fichier data.csv
def getData(label_encoder):
    df = pd.read_csv('archive/data.csv')
#Avant avril 2001, il n'y avait presque aucune règle dans l'UFC (pas de juges, pas de limites de temps, pas de rounds, 
#etc.). C'est à partir de cette date précise que l'UFC a commencé à mettre en place un ensemble de règles connu sous 
#le nom de "Unified Rules of Mixed Martial Arts". Par conséquent, nous supprimons tous les combats antérieurs 
#à cette mise à jour majeure de l'histoire des règles de l'UFC.
    df = df.loc[df['date'] > '2001-04-01', columns]
# Copy le dataframe pour le traiter dans une autre fonction
    dfc = df.copy()
# Permet d'encoder la totalité des valeurs string en index (Int)
    for column in df.select_dtypes(include=['object']).columns:
        # Encode pour chaque colonne de type chaînes de caractères en valeur numérique
        df[column] = label_encoder.fit_transform(df[column])

    df = df.apply(swap_values, axis=1)
# Drop de la colonne 'Winner' pour effectuer l'apprentissage
    X=df.drop('Winner', axis=1)
# Mise en place du y, montrant la réponse aux prédictions attendus
    y=df['Winner']
    return X,y,dfc

