from startModel import *
from cleanData import *
from sklearn.model_selection import train_test_split
from analyse import *
import sys
# Définition d'un label encoder pour changer les string en index
label_encoder = LabelEncoder()
# Récupération des valeurs X,y et d'un dataframe inchangé
X, y, df = getData(label_encoder)
# Séparation des différentes parties attendus pour faire le test et l'apprentissage du model sélectionnée
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=50)

model = startRandomForest(X_train, X_test, y_train, y_test)
#model=startKNN(X_train,X_test,y_train,y_test)
#model=startSVM(X_train,X_test,y_train,y_test)
#model=startDecisionTree(X_train,X_test,y_train,y_test)
#model=startLogisticRegression(X_train,X_test,y_train,y_test)
#model=startLinearSVC(X_train,X_test,y_train,y_test)
#model=startNaiveBayes(X_train,X_test,y_train,y_test)

# Insertion des valeurs pour la prédiction
combattantR = input("Combattant Rouge ?: ")
combattantB = input("Combattant Bleu ?: ")
poids = input("Poids ?: ")
belt = input("Ceinture ?: ")

# Récupération des valeurs indispensables pour la prédiction
fr=getFighterStats(df, label_encoder, combattantR)
fb=getFighterStats(df, label_encoder, combattantB)

# Prédiction auprès du model sélectionnée
winner = predict(fr, fb, belt, model, poids)

# Affichage du gagnant prédit
if winner == 0:
    print(combattantR)
else:
    print(combattantB)