from models import *
from analyse import *

#Fonction permettant de calculer la matrice de confusion et les données liées à un entrainement et un test d'un model
def calculateMatrix(y_true, y_pred):
    accuracy = accuracy_score(y_true, y_pred)
    confusion_matrix_result = confusion_matrix(y_true, y_pred)
    classification_report_result = classification_report(y_true, y_pred, zero_division=1)
    return accuracy, confusion_matrix_result, classification_report_result

#Fonction d'affichage de la matrice de confusion
def seeMatrix(matrix, classes):
    cmap = plt.cm.Blues
    disp = ConfusionMatrixDisplay(confusion_matrix=matrix, display_labels=classes)
    disp.plot(cmap=cmap)
    plt.show()

#Fonction affichant les données clés à un model
def report(accuracy,confMatrix,classReport):
    print(f'Accuracy: {accuracy}')
    print(f'Confusion Matrix:\n{confMatrix}')
    print(f'Classification Report:\n{classReport}')

#Fonction afin de récupérer le randomForest entrainé et savoir les donneés clés de l'entrainement
def startRandomForest(X_train,X_test,y_train,y_test):
    y_pred, rf = RandomForest(X_train, X_test, y_train)
    rf_ac, rf_matrix, rf_class_report = calculateMatrix(y_test, y_pred)
    report(rf_ac, rf_matrix, rf_class_report)
    #seeMatrix(rf_matrix, rf.classes_)
    return rf

#Fonction afin de récupérer le KNN entrainé et savoir les donneés clés de l'entrainement
def startKNN(X_train,X_test,y_train,y_test):
    y_pred, knn = KNN(X_train, X_test, y_train)
    knn_ac, knn_matrix, knn_class_report = calculateMatrix(y_test, y_pred)
    report(knn_ac, knn_matrix, knn_class_report)
    #seeMatrix(knn_matrix, knn.classes_)
    return knn

#Fonction afin de récupérer le SVM entrainé et savoir les donneés clés de l'entrainement
def startSVM(X_train,X_test,y_train,y_test):
    y_pred, svm = SVM(X_train, X_test, y_train)
    svm_ac, svm_matrix, svm_class_report = calculateMatrix(y_test, y_pred)
    report(svm_ac, svm_matrix, svm_class_report)
    #seeMatrix(svm_matrix, svm.classes_)
    return svm

#Fonction afin de récupérer le DecisionTree entrainé et savoir les donneés clés de l'entrainement
def startDecisionTree(X_train,X_test,y_train,y_test):
    y_pred, dt = DecisionTree(X_train, X_test, y_train)
    dt_ac, dt_matrix, dt_class_report = calculateMatrix(y_test, y_pred)
    report(dt_ac, dt_matrix, dt_class_report)
    #seeMatrix(dt_matrix, dt.classes_)
    return dt

#Fonction afin de récupérer la LogisticRegression entrainée et savoir les donneés clés de l'entrainement
def startLogisticRegression(X_train,X_test,y_train,y_test):
    y_pred, lr = LogisticRegress(X_train, X_test, y_train)
    lr_ac, lr_matrix, lr_class_report = calculateMatrix(y_test, y_pred)
    report(lr_ac, lr_matrix, lr_class_report)
    #seeMatrix(lr_matrix, lr.classes_)
    return lr

#Fonction afin de récupérer la SVC Linear entrainée et savoir les donneés clés de l'entrainement
def startLinearSVC(X_train,X_test,y_train,y_test):
    y_pred, svc = Linearsvc(X_train, X_test, y_train)
    svc_ac, svc_matrix, svc_class_report = calculateMatrix(y_test, y_pred)
    report(svc_ac, svc_matrix, svc_class_report)
    #seeMatrix(svc_matrix, svc.classes_)
    return svc

#Fonction afin de récupérer la Gaussian Naive Bayes entrainée et savoir les donneés clés de l'entrainement
def startNaiveBayes(X_train,X_test,y_train,y_test):
    y_pred, gnb = GaussianNaiveBayes(X_train, X_test, y_train)
    gnb_ac, gnb_matrix, gnb_class_report = calculateMatrix(y_test, y_pred)
    report(gnb_ac, gnb_matrix, gnb_class_report)
    #seeMatrix(gnb_matrix, gnb.classes_)
    return gnb

