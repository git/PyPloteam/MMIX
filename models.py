from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import SGDClassifier
from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import GaussianNB

# Définition d'un model type RandomForest et entrainement sur les données passées en paramètre
def RandomForest(X_train, X_test, y_train):
    random_forest = RandomForestClassifier(
            n_estimators=100,  # Nombre d'arbres dans la forêt
            criterion='entropy',  # Critère pour diviser les nœuds 
            max_depth=10,  # Profondeur maximale des arbres
            min_samples_split=2,  # Nombre minimal d'échantillons requis pour diviser un nœud
            min_samples_leaf=1,  # Nombre minimal d'échantillons requis pour être une feuille
            random_state=0  # Contrôle la randomisation
    )
    # Entraînement du classificateur sur les données d'entraînement       
    random_forest.fit(X_train, y_train)
    return  random_forest.predict(X_test), random_forest

# Définition d'un model type KNN et entrainement sur les données passées en paramètre
def KNN(X_train, X_test, y_train):
    knn = KNeighborsClassifier(
        n_neighbors=5  # Nombre de voisins à utiliser
    )
    knn.fit(X_train, y_train)
    return knn.predict(X_test),knn

# Définition d'un model type SVM et entrainement sur les données passées en paramètre
def SVM(X_train, X_test, y_train):
    clf = svm.SVC(
        gamma=0.001  # Paramètre de noyau
    )
    clf.fit(X_train,y_train)
    return clf.predict(X_test),clf

# Définition d'un model type Arbre de décision et entrainement sur les données passées en paramètre
def DecisionTree(X_train, X_test, y_train):
    decisionTree = DecisionTreeClassifier()
    decisionTree = decisionTree.fit(X_train,y_train)
    return decisionTree.predict(X_test),decisionTree

# Définition d'un model type regression logistique et entrainement sur les données passées en paramètre
def LogisticRegress(X_train, X_test, y_train):
    logistic = LogisticRegression()
    logistic.fit(X_train,y_train)
    return logistic.predict(X_test),logistic

# Définition d'un model type SVC lineaire et entrainement sur les données passées en paramètre
def Linearsvc(X_train, X_test, y_train):
    svc = LinearSVC(
        C=1.0,  # Paramètre de régularisation 
        dual=False,  # Utilisation de la forme duale 
        verbose=True,  # Affichage des détails de l'optimisation
        loss="squared_hinge",  # Fonction de perte utilisée
        multi_class="crammer_singer"  # Méthode pour résoudre les problèmes de classification multi-classes
    )
    svc.fit(X_train,y_train)
    return svc.predict(X_test),svc

# Définition d'un model type Gaussian Naive Bayes et entrainement sur les données passées en paramètre
def GaussianNaiveBayes(X_train, X_test, y_train):
    gnb = GaussianNB()
    gnb.fit(X_train, y_train)
    return gnb.predict(X_test),gnb