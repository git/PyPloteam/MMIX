# Introduction

Voici MMIX, une application IA permettant de prédire à hauteur de 57% le vainqueur d'un combat entre deux combattants MMA à l'UFC. 

Les statistiques sont issues d'un dataframe disponible sur [Kaggle](https://www.kaggle.com/datasets/rajeevw/ufcdata).

Le model utilisé est une forêt aléatoire.

# Comment lancer le projet ?

Pour lancer le projet, il faut simplement exécuter la commande : `python3 main.py`

Aucun menu n'est disponible pour contrôler le modèle, il faut juste décommenter la ligne dans le main pour le modèle souhaité.

Ensuite, une saisie devra être effectué afin de renseigner les 2 combattants, la catégorie et si c'est un combat pour la ceinture.

Voici un exemple si besoin afin de ne pas chercher dans le data.csv:
    -   Ciryl Gane
    -   Jon Jones
    -   Heavyweight
    -   1

Combat indisponible dans le dataset mais qui a été effectuée à l'UFC.

[Gane VS Jones](https://www.ufc.com/event/ufc-285#10328)

# Les données

Les données utilisées sont issu d'un dataframe reprenant les statistiques de la plus grande organisation MMA au monde, l'UFC (Ultimate Fighting Championship).

## Qu'est ce que le MMA ?

Le MMA, ou arts martiaux mixtes, est un sport de percussion-préhension (debout et au sol). 

C'est un sport mis en avant par la médiatisation, comme le catch il y a quelques années. Il s'est démocratisé il y a peu de temps en France, car il n'est autorisé que depuis 2020.

Avant cette date, ce sport était pratiqué par nos Français à l'extérieur de l'hexagone.

Ce sport est très complexe, et seuls les combattants maîtrisant plusieurs styles de combat peuvent gravir les échelons.

Le MMA permet de réunir plein de sortes de styles de combat comme la boxe anglaise, le Jiu-jitsu brésilien, la lutte et le sambo.

Le MMA, comme en boxe, permet de combattre seulement contre sa catégorie de poids, sauf exception (devenir double champion, montée de catégories...).

## Et nos Français ?

Actuellement à l'UFC, nous possédons 4 dans le top 15 mondial de l'UFC.

[Ciryl Gane](https://www.ufc.com/athlete/ciryl-gane) - Top 2 dans la catégorie Poids Lourd Homme

[Manon Fiorot](https://www.ufc.com/athlete/manon-fiorot) - Top 3 dans la catégorie Poids Mouche Femme

[Nassourdine Imavov](https://www.ufc.com/athlete/nassourdine-imavov) - Top 8 dans la catégorie Poids Moyen Homme

[Benoit Saint Denis](https://www.ufc.com/athlete/mariya-agapova-0) - Top 12 dans la catégorie Poids Léger Homme, il est en série de 3 victoires très impressionnantes et va combattre début avril contre le Top 3 [Dustin Poirier](https://www.ufc.com/athlete/dustin-poirier) qui est une légende du MMA

## Que retenir du dataset ?

Le dataset contient 140 colonnes, il faut donc trier afin de récupérer seulement les informations importantes:

 - **B_avg_BODY_landed** : Nombre moyen de coups au corps réussis par le combattant du coin bleu

- **B_avg_HEAD_landed** : Nombre moyen de coups à la tête réussis par le combattant du coin bleu

- **B_avg_TD_att** : Nombre moyen de tentatives de takedown par le combattant du coin bleu

- **B_avg_TOTAL_STR_landed** : Nombre moyen total de coups réussis par le combattant du coin bleu

- **B_avg_opp_BODY_att** : Nombre moyen de tentatives de coups au corps par les adversaires contre le combattant du coin bleu

- **B_avg_opp_HEAD_landed** : Nombre moyen de coups à la tête réussis par les adversaires contre le combattant du coin bleu

- **B_avg_opp_LEG_landed** : Nombre moyen de coups à la jambe réussis par les adversaires contre le combattant du coin bleu

- **B_avg_opp_SIG_STR_att** : Nombre moyen de tentatives de coups significatifs par les adversaires contre le combattant du coin bleu

- **B_avg_opp_TOTAL_STR_att** : Nombre moyen total de tentatives de coups par les adversaires contre le combattant du coin bleu

- **R_avg_TD_att** : Nombre moyen de tentatives de takedown par le combattant du coin rouge

- **R_avg_opp_GROUND_att** : Nombre moyen de tentatives de coups au sol par les adversaires contre le combattant du coin rouge

- **R_avg_opp_SIG_STR_landed** : Nombre moyen de coups significatifs réussis par les adversaires contre le combattant du coin rouge


## Preparation des données

Pour rappel, notre dataset présentait un large pannel de données. Ces données-là sont chargées à partir d'un fichier CSV contenant des informations sur les combats UFC, telles que les noms des combattants, les statistiques de combat, la date du combat, le gagnant, etc.

La première étape a donc été de nettoyer ses données : 

### Nettoyage des données 

- Les combats antérieurs à avril 2001, avant l'implémentation des règles unifiées des arts martiaux mixtes, sont supprimés car ils pourraient ne pas être représentatifs des combats actuels. On parle d'une époque où il n'y avait ni juge, ni limite de temps et rounds.

- Les variables catégorielles sous formes de chaînes de caractères dans le jeu de données ont eu besoin d'être encodées en valeurs numériques à l'aide de la méthode d'encodage de label. Cette étape permet de convertir les chaînes de caractères en nombres, ce qui est nécessaire pour que nos modèles d'apprentissage automatique puissent traiter efficacement les données.

- Une fonction swap_values est appliquée à chaque ligne du jeu de données. Cette fonction permet l'échange de données entre le combattant bleue et le combattant rouge. Nous avons tenté cela pour essayer de baisser l'influence trop importante qu'avait pour un combattant d'être rouge, car statistiquement ce côté gagne bien plus souvent que le côté bleu, ce qui influençait grandement nos predictions.

# Model

### Les tests de modèles

Dans le processus de choix du modèle le plus approprié pour notre tâche de prédiction des résultats de MMA, nous avons expérimenté plusieurs algorithmes d'apprentissage. Voici une analyse des différents choix tentés et des raisons pour lesquelles le modèle Random Forest a été jugé le plus intéressant :


1. K-Nearest Neighbors (KNN) :
- Avantages :
- - Facilité d'implémentation : KNN est simple à mettre en œuvre et à comprendre
- - Adaptabilité : Il peut gérer efficacement les problèmes de classification non linéaires, ce qui peut être pertinent pour les combats MMA où les interactions entre les caractéristiques peuvent être complexes.
- - Robustesse aux données bruitées : KNN peut fonctionner de manière robuste même en présence de données bruitées, ce qui peut être utile compte tenu de la variabilité des performances des combattants.
- Inconvénients :
- - Sensibilité à la distance : Les performances de KNN dépendent fortement du choix de la mesure de distance et du nombre de voisins, ce qui peut être difficile à déterminer dans un contexte MMA.

2. Régression Logistique :
- Avantages :
- - Interprétabilité : La régression logistique fournit des coefficients qui peuvent être interprétés directement, ce qui peut être utile pour comprendre l'importance relative des caractéristiques dans la prédiction.
- - Facilité d'ajustement : Elle est rapide à entraîner et à prédire
- Inconvénients :
- - Limitation à la linéarité : La régression logistique est limitée par sa capacité à capturer des relations linéaires entre les caractéristiques et la cible, ce qui peut être insuffisant pour modéliser les relations complexes présentes dans les données MMA.
- - Sensibilité aux valeurs aberrantes : Elle peut être sensible aux valeurs aberrantes, ce qui peut affecter ses performances, et dans le contexte MMA, où les performances des combattants peuvent varier considérablement.
3. Random Forest :
- Avantages (comparatif) :
- - Capacité à capturer des interactions non linéaires : Contrairement à la régression logistique, Random Forest peut gérer efficacement les interactions non linéaires entre les caractéristiques, ce qui est crucial pour modéliser les relations complexes dans nos données.
- - Robustesse au surajustement : Contrairement à KNN, qui peut être sensible au surajustement avec de grandes quantités de données, Random Forest est moins susceptible de surajuster grâce à l'agrégation des prédictions de multiples arbres de décision.  

### Choix du Modèle : 

Pour prédire les résultats des combats UFC, nous optons pour un modèle de classification, étant donné que notre objectif est de catégoriser les combats en fonction du vainqueur (classe). L'Arbre de Décision Aléatoire (Random Forest) a une capacité à gérer efficacement des ensembles de données complexes et à fournir des prédictions précises tout en minimisant le surapprentissage.
Dans le contexte de la prédiction des résultats des combats UFC, la classification est plus appropriée que la régression car nous cherchons à attribuer une étiquette de classe discrète à chaque combat, telle que "Gagnant: Rouge" ou "Gagnant: Bleu". La régression, en revanche, est utilisée pour prédire des valeurs continues, ce qui ne correspond pas au problème de prédiction des résultats des combats UFC. La classification consiste à assigner des étiquettes de classe à des données en fonction de leurs caractéristiques.

### Fonctionnement : 
Dans un modèle de classification comme les Random Forest, chaque arbre de décision produit une prédiction de classe pour chaque combattant dans un combat donné. Dans le cas d'une prédiction binaire (gagnant ou perdant), chaque arbre attribue un point au combattant qu'il prédit comme gagnant et aucun point à l'autre combattant.

Ensuite, pour obtenir la prédiction finale, les points attribués par chaque arbre sont agrégés. Par exemple, si un combat est évalué par 100 arbres et que 70 arbres prédisent le combattant A comme gagnant et 30 arbres prédisent le combattant B comme gagnant, alors le modèle prédit que le combattant A sera le gagnant avec 70 points contre 30 pour le combattant B.

Cela permet de prendre en compte les opinions de multiples arbres de décision dans la prédiction finale, ce qui rend le modèle plus robuste et moins sensible aux variations aléatoires dans les données d'entraînement.

### Justification du Choix :
L'Arbre de Décision Aléatoire est un modèle polyvalent qui s'adapte bien à une grande variété de problèmes de classification. Il fonctionne en construisant un grand nombre d'arbres de décision individuels, puis en combinant leurs prédictions pour obtenir une prédiction finale. Cette approche permet de capturer des interactions complexes entre les différentes caractéristiques des combats UFC, tout en réduisant le risque de surapprentissage par agrégation.

### Description des Paramètres :
- n_estimators : Le nombre d'arbres de décision dans la forêt aléatoire. Plus le nombre d'estimateurs est élevé, plus le modèle aura de stabilité et de précision, bien qu'il puisse nécessiter plus de temps de calcul.
- criterion : Le critère utilisé pour mesurer la qualité de la division des nœuds de l'arbre. Pour la classification binaire des résultats UFC, nous utilisons généralement "gini" ou "entropy" pour évaluer la pureté des nœuds en fonction de la distribution des classes.
- max_depth : La profondeur maximale de chaque arbre de décision. Limiter la profondeur peut aider à prévenir le surapprentissage en restreignant la complexité de l'arbre, mais peut également réduire sa capacité à capturer des relations plus complexes dans les données.
- min_samples_split : Le nombre minimum d'échantillons requis pour diviser un nœud interne. Cela permet de contrôler la croissance de l'arbre en évitant les divisions qui ne seraient pas significatives en raison d'un faible nombre d'échantillons.
- min_samples_leaf : Le nombre minimum d'échantillons requis pour former une feuille de l'arbre. Cela aide à contrôler la complexité de l'arbre en empêchant la formation de feuilles avec un faible nombre d'échantillons, ce qui peut améliorer la généralisation du modèle.
- random_state : Une graine aléatoire utilisée pour initialiser la génération de nombres aléatoires. Cela assure la reproductibilité des résultats, permettant ainsi de comparer les performances du modèle de manière cohérente.

En utilisant ces paramètres et en ajustant les hyperparamètres de manière appropriée, notre modèle d'Arbre de Décision Aléatoire peut être entraîné pour fournir des prédictions précises des résultats des combats UFC, offrant ainsi une solution robuste pour la classification des données complexes dans ce domaine.

# Améliorations

- Remplacer les valeurs NaN de façon plus pertinente, en effet pour un combattant qui n'a pas de données de poid, plutôt que de lui accorder le poid médian de tout les combatants existants, il serait plus cohérent de lui attribuer le poid médian des combattants de la catégorie de poid où il a combattu par exemple, ou encore se fier à un combat où son pied est spécifié.

- De nombreux combats ne sont pas renseignés dans ce dataset, ajouter une autre source de données serait peut être plus bénéfique car actuellement le modèle le plus élévé en accuracy est de 57%, ce qui est très peu.

- Ajouter d'autre datasets issu de plusieurs organisations de MMA pourrait être un bon ajout pour faire combattre des personnes qui ne sont pas dans la même organisation et ainsi avoir des combats plus improbable et moins prédictable. 