from sklearn.metrics import accuracy_score, confusion_matrix, classification_report, ConfusionMatrixDisplay, roc_curve, auc, RocCurveDisplay
import matplotlib.pyplot as plt
from sklearn.model_selection import learning_curve
import numpy as np
from sklearn import metrics
from sklearn.preprocessing import LabelEncoder
from cleanData import *
import sys

# Fonction permettant de récupérer la moyenne de statistiques des combats précédents
def getFighterStats(df, label_encoder, fighter_name):
    # Définition des colonnes ou la moyenne sera appliquée
    columns = ['B_avg_BODY_landed', 'B_avg_HEAD_landed', 'B_avg_TD_att', 'B_avg_TOTAL_STR_landed', 
                'B_avg_opp_BODY_att', 'B_avg_opp_HEAD_landed', 'B_avg_opp_LEG_landed', 
                'B_avg_opp_SIG_STR_att', 'B_avg_opp_TOTAL_STR_att'] 
    # Tentative de récupération du dataframe ou le fighter passée en paramètre combat
    df_temp = df[(df['R_fighter'] == fighter_name) | (df['B_fighter'] == fighter_name)]
    # Gestion d'erreur si le df est vide
    if df_temp.empty:
        print(f"{fighter_name} introuvable. Abandon de l'application")
        sys.exit(1)
    # sous-fonction permettant d'inverser le coté du combattant
    def swap_values_if_needed(row):
        if row['R_fighter'] == fighter_name:
            return swap_values_withoutran(row)
        return row
    # Permet de faire un foreach et d'appliquer la fonction swap_values_if_needed pour chaque ligne
    df_temp = df_temp.apply(swap_values_if_needed, axis=1)
    # Fait la moyenne des colonnes précédement renseignée dans la liste columns
    return df_temp[columns].mean()

def predict(fighterStatsR,fighterStatsB,titlebout,model,weight):
    # Définition des colonnes attendus pour la prédiction
    columns = ['B_fighter','R_fighter','title_bout',
                    'B_avg_BODY_landed', 'B_avg_HEAD_landed', 'B_avg_TD_att', 'B_avg_TOTAL_STR_landed', 
                'B_avg_opp_BODY_att', 'B_avg_opp_HEAD_landed', 'B_avg_opp_LEG_landed', 
                'B_avg_opp_SIG_STR_att', 'B_avg_opp_TOTAL_STR_att',
                
                'R_avg_BODY_landed', 'R_avg_HEAD_landed', 'R_avg_TD_att', 'R_avg_TOTAL_STR_landed', 
                'R_avg_opp_BODY_att', 'R_avg_opp_HEAD_landed', 'R_avg_opp_LEG_landed', 
                'R_avg_opp_SIG_STR_att', 'R_avg_opp_TOTAL_STR_att','weight_class'] 

    # Définition d'un dataframe issu des colonnes précedemment renseignée
    df = pd.DataFrame(columns=columns)
    # Association des valeurs liées au deux combattants pour la prédiction
    fight = {
        
        'B_fighter':0,'R_fighter':0,'title_bout':1,
        'B_avg_BODY_landed': fighterStatsB['B_avg_BODY_landed'],
        'B_avg_HEAD_landed': fighterStatsB['B_avg_HEAD_landed'], 
        'B_avg_TD_att': fighterStatsB['B_avg_TD_att'],
        'B_avg_TOTAL_STR_landed': fighterStatsB['B_avg_TOTAL_STR_landed'],
        'B_avg_opp_BODY_att': fighterStatsB['B_avg_opp_BODY_att'],
        'B_avg_opp_HEAD_landed': fighterStatsB['B_avg_opp_HEAD_landed'],
        'B_avg_opp_LEG_landed': fighterStatsB['B_avg_opp_LEG_landed'],
        'B_avg_opp_SIG_STR_att': fighterStatsB['B_avg_opp_SIG_STR_att'],
        'B_avg_opp_TOTAL_STR_att': fighterStatsB['B_avg_opp_TOTAL_STR_att'],

        'R_avg_BODY_landed': fighterStatsR['B_avg_BODY_landed'],
        'R_avg_HEAD_landed': fighterStatsR['B_avg_HEAD_landed'], 
        'R_avg_TD_att': fighterStatsR['B_avg_TD_att'],
        'R_avg_TOTAL_STR_landed': fighterStatsR['B_avg_TOTAL_STR_landed'],
        'R_avg_opp_BODY_att': fighterStatsR['B_avg_opp_BODY_att'],
        'R_avg_opp_HEAD_landed': fighterStatsR['B_avg_opp_HEAD_landed'],
        'R_avg_opp_LEG_landed': fighterStatsR['B_avg_opp_LEG_landed'],
        'R_avg_opp_SIG_STR_att': fighterStatsR['B_avg_opp_SIG_STR_att'],
        'R_avg_opp_TOTAL_STR_att': fighterStatsR['B_avg_opp_TOTAL_STR_att'],

        'weight_class': 1
    }
    # Ajout des valeurs dans le dataframe
    df = df._append(fight, ignore_index=True)
    # Retourne la valeur 'Winner' suite à la prédiction du model choisis
    return model.predict(df)